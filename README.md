# README #

This project compares the transactions in two CSV files. On upload of the two files for comparison it initially responds with a summary result indicating matched, unmatched and duplicate transactions. Users can then run deeper analysis using three different matching algorithms and the server will recommend potential matches for unmatched transactions if they exist and highlight any still unmatched transactions and any duplicate entries.

### Note ###

While this application makes use of HTTP to expose it's interfaces it should not be considered truly RESTftul, for example it is not HATEOAS compliant.

### Assumptions ###

1. This application has been tested on JBoss EAP 6 (JBoss 7). 
2. GIT, Maven and Java are prerequisites. (Java 8 is a requirement if running the [Java Client](https://bitbucket.org/bgunning/tutuka-java-client) )
3. Users require two CSV files in correct format to compare. Two examples in exist in `src/test/resources`, `ClientMarkoffFile20140113.csv` and `TutukaMarkoffFile20140113.csv`. The files are also available on the [Downloads](https://bitbucket.org/bgunning/brian-project/downloads/) page.

### How do I get set up? ###

* `git pull`
* `mvn clean install`
* `cp <src_directory>target/TutukaCompare2.war <JBOSS_HOME>/standalone/deployments`

### How do I use ###

1. Web based UI. A running server is available on request.
2. Java Client
    * Code available here [Bitbucket](https://bitbucket.org/bgunning/tutuka-java-client) 
    * Download the compiled jar here: [Downloads](https://bitbucket.org/bgunning/tutuka-server/downloads/)
3. HTTP Client
    * Download requests for Insomnia HTTP Client here [Downloads](https://bitbucket.org/bgunning/tutuka-server/downloads/)
    * Download Insomnia HTTP Client here: [ Insomnia ](https://insomnia.rest/)
4. cURL
    * Sample requests below

#### Using The UI ####

1. Upload two files via upload form
2. The Summary Table is populated with one entry detailing summary results of the file comparison.
3. Run any of the matching algorithms by pressing either "Reconcile Basic", "Reconcile Fuzzy"  or "Reconcile TransactionID" buttons
4. The "Matched", "Unmatched" and "Duplicate" tables will then be updated based on algorithm results.
5. You can run all algorithms as often as you wish and each time the tables will be updated accordingly.

#### Using The Java Client ####
1. Please see README here:  [Bitbucket](https://bitbucket.org/bgunning/tutuka-java-client)

#### Using cURL ####

1. Upload files
    * ``` curl --request POST --url http://myserver.com/tutuka/api/v1/upload/files --header 'content-type: multipart/form-data; --form file1=@/home/path/ClientMarkoffFile20140113.csv --form file2=@/home/path/TutukaMarkoffFile20140113.csv ```


2. Run matching algorithms
    * Basic Algorithm
      * ```curl --request GET --url http://myserver.com/tutuka/api/v1/reconcile/column/{id}```
    * Fuzzy Logic Algorithm
      * ```curl --request GET --url http://myserver.com/tutuka/api/v1/reconcile/fuzzy/{id}/{FUZZY_LOGIC_RATIO}```
      * Fuzzy Logic ratio recommended value is 90
    * Transaction ID Algorithm
      *``` curl --request GET --url http://myserver.com/tutuka/api/v1/reconcile/transaction/{id} ```
      
#### Using HTTP Client ####
1. Download Insomnia Client here: [ Insomnia ](https://insomnia.rest/)
2. Download HTTP requests here: [Downloads](https://bitbucket.org/bgunning/brian-project/downloads/)
3. Follow the same procedure for web ui, java client or cURL


#### The Algorithms ####

* Basic Algorithm

If the server fails to find a perfect match for a transaction the basic algorithm will recommend a potential match if 7 out of the 8 available transactions match.
**Design Decison** Matching transactions with more than one column not matching was deemed too risky for an automatic match recomendation.

* Fuzzy Logic

The Fuzzy Logic algorithm uses Levenshtein distance to calculate similarity between strings. This application uses the [JavaWuzzy ](https://github.com/xdrop/fuzzywuzzy) library for it's fuzzy logic engine. This is a Java implementation of the Python FuzzyWuzzy library. When using the Java client users are free to select any "fuzzy ratio" to investigate various matches. The UI is configured to use ```90``` by default. Values below ```90``` have been seen to produce erratic results for this use case and values closer to ```100``` often results in algorithm output that is too strict to generate any matches. A ratio of ```100``` represents a perfect match. Users of the java client are free to choose any ```fuzzy ratio``` they wish via the ```fuzzyratio pathparam```, e.g. ```tutuka/api/v1/reconcile/fuzzy/{id}/{FUZZY_LOGIC_RATIO}```

* TransctionID

This algorithm focuses on the column that makes a transaction unique, it's TransactionID. If this algorithm detects unmatched transactions with the same transaction id it will recommend these as a potential match.

### Running example ###

A cloud image is available for use, please request.

### Note ###

All file comparison results are stored in a simple cache  quick for later retrieval. This is a light wrapper on a Java HashMap. This in stored ephemerally and will not survive reboot. Similarly, as this is a simple wrapper on a HashMap it does not have any draining or TTL features.

