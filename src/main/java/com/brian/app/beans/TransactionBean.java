package com.brian.app.beans;

import com.brian.app.utilities.SanitizeStringConverter;
import com.brian.app.utilities.StringConstants;
import com.univocity.parsers.annotations.*;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A java representation of a transaction in an uploaded CSV file
 */
public class TransactionBean {


    @Trim
    @NullString(nulls = { "?", "-" })
    @Parsed(defaultNullRead = "UNKNOWN")
    private String profileName;

    @Format(formats = {"yyyy-MM-dd HH:mm:ss"})
    @Parsed
    private Date transactionDate;


    @Parsed
    private long transactionAmount;

    @Convert(conversionClass = SanitizeStringConverter.class)
    @Trim
    @Parsed
    private String transactionNarrative;

    @Trim
    @Parsed
    private String transactionDescription;

    @Parsed
    private long transactionID;

    @Parsed
    private int transactionType;

    @Parsed
    private String walletReference;


    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(final String profileName) {
        this.profileName = profileName;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(final Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(final long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionNarrative() {
        return transactionNarrative;
    }

    public void setTransactionNarrative(final String transactionNarrative) {

        this.transactionNarrative = transactionNarrative;

    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(final String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public long getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(final long transactionID) {
        this.transactionID = transactionID;
    }

    public int getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(final int transactionType) {
        this.transactionType = transactionType;
    }

    public String getWalletReference() {
        return walletReference;
    }

    public void setWalletReference(final String walletReference) {
        this.walletReference = walletReference;
    }

//    @Override
//    public String toString() {
//        return "Transaction:{transactionDate=" + transactionDate + ":transactionAmount=" + transactionAmount + ":transactionNarrative=" + transactionNarrative + ":transactionDescription=" + transactionDescription + ":transactionID=" + transactionID + ":transactionType=" + transactionType +"}";
//    }


    @Override
    public String toString() {
        return "Transaction{" +
                "profileName='" + profileName + '\'' +
                ", transactionDate=" + transactionDate +
                ", transactionAmount=" + transactionAmount +
                ", transactionNarrative='" + transactionNarrative + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", transactionID=" + transactionID +
                ", transactionType=" + transactionType +
                ", walletReference='" + walletReference + '\'' +
                '}';
    }

    public String toSanatizedString(){

        return sanatise(this.toString());

    }


    private String sanatise(final String theString) {
        final Pattern pattern = Pattern.compile(StringConstants.REGEX_A_Z_A_Z0_9);
        String aString=theString;
        final Matcher match = pattern.matcher(aString);
        while (match.find()) {
            final String s = match.group();
            aString = aString.replaceAll(String.format(StringConstants.REGEX_PERCENTAGE_S, s), "");
        }
        return aString;
    }

    /**
     * Check for equals
     * @param other - the object being compared against
     * @return true if equals
     */
    @Override
    public boolean equals(final Object other){
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof TransactionBean))return false;
        final TransactionBean otherMyClass = (TransactionBean) other;
        return otherMyClass.toString().equalsIgnoreCase(this.toString());
    }

    /**
     * When overriding equals() must override hashcode() as well.
     * @return hashcode
     */
    @Override
    public int hashCode() {
        final int prime = 9;
        int hashCode = 1;
        hashCode = prime * hashCode
                + ((toString() == null) ? 0 : toString().hashCode());
        return hashCode;
    }


    /**
     * Check if any 7 of the 8 columns match
     * @param other - the object being compared against
     * @return - true if equals
     */
    public boolean partialEquals(final Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof TransactionBean)) return false;
        final TransactionBean otherMyClass = (TransactionBean) other;
        return otherMyClass.partialWithoutDate().equalsIgnoreCase(this.partialWithoutDate())
                || otherMyClass.partialWithoutAmount().equalsIgnoreCase(this.partialWithoutAmount())
                || otherMyClass.partialWithoutNarrative().equalsIgnoreCase(this.partialWithoutNarrative())
                || otherMyClass.partialWithoutDescription().equalsIgnoreCase(this.partialWithoutDescription())
                || otherMyClass.partialWithoutId().equalsIgnoreCase(this.partialWithoutId())
                || otherMyClass.partialWithoutType().equalsIgnoreCase(this.partialWithoutType())
                || otherMyClass.partialWithoutReference().equalsIgnoreCase(this.partialWithoutReference())
                || otherMyClass.partialWithoutName().equalsIgnoreCase(this.partialWithoutName());
    }


    /**
     * Implementation of the partialEquals methods below
     * @return - All return true if equals
     */
    public String partialWithoutName() {
        return "Transaction{"+
                ", transactionDate=" + transactionDate +
                ", transactionAmount=" + transactionAmount +
                ", transactionNarrative='" + transactionNarrative + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", transactionID=" + transactionID +
                ", transactionType=" + transactionType +
                ", walletReference='" + walletReference + '\'' +
                '}';
    }


    public String partialWithoutDate() {
        return "Transaction{" +
                "profileName='" + profileName + '\'' +
                ", transactionAmount=" + transactionAmount +
                ", transactionNarrative='" + transactionNarrative + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", transactionID=" + transactionID +
                ", transactionType=" + transactionType +
                ", walletReference='" + walletReference + '\'' +
                '}';
    }


    public String partialWithoutAmount() {
        return "Transaction{" +
                "profileName='" + profileName + '\'' +
                ", transactionDate=" + transactionDate +
                ", transactionNarrative='" + transactionNarrative + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", transactionID=" + transactionID +
                ", transactionType=" + transactionType +
                ", walletReference='" + walletReference + '\'' +
                '}';
    }


    public String partialWithoutNarrative() {
        return "Transaction{" +
                "profileName='" + profileName + '\'' +
                ", transactionDate=" + transactionDate +
                ", transactionAmount=" + transactionAmount +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", transactionID=" + transactionID +
                ", transactionType=" + transactionType +
                ", walletReference='" + walletReference + '\'' +
                '}';
    }


    public String partialWithoutDescription() {
        return "Transaction{" +
                "profileName='" + profileName + '\'' +
                ", transactionDate=" + transactionDate +
                ", transactionAmount=" + transactionAmount +
                ", transactionNarrative='" + transactionNarrative + '\'' +
                ", transactionID=" + transactionID +
                ", transactionType=" + transactionType +
                ", walletReference='" + walletReference + '\'' +
                '}';
    }



    public String partialWithoutId() {
        return "Transaction{" +
                "profileName='" + profileName + '\'' +
                ", transactionDate=" + transactionDate +
                ", transactionAmount=" + transactionAmount +
                ", transactionNarrative='" + transactionNarrative + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", transactionType=" + transactionType +
                ", walletReference='" + walletReference + '\'' +
                '}';
    }


    public String partialWithoutType() {
        return "Transaction{" +
                "profileName='" + profileName + '\'' +
                ", transactionDate=" + transactionDate +
                ", transactionAmount=" + transactionAmount +
                ", transactionNarrative='" + transactionNarrative + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", transactionID=" + transactionID +
                ", walletReference='" + walletReference + '\'' +
                '}';
    }


    public String partialWithoutReference() {
        return "Transaction{" +
                "profileName='" + profileName + '\'' +
                ", transactionDate=" + transactionDate +
                ", transactionAmount=" + transactionAmount +
                ", transactionNarrative='" + transactionNarrative + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", transactionID=" + transactionID +
                ", transactionType=" + transactionType +
                ", walletReference='" + walletReference + '\'' +
                '}';
    }




}
