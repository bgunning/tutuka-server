package com.brian.app.cache;


import com.brian.app.annotationinterfaces.Simple;
import com.brian.app.responses.ResponseBuilder;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.logging.Logger;

@Simple
@Singleton
public class SimpleCache implements Cache {

    private HashMap simCache;
    @Inject
    private
    Logger logger;

    @PostConstruct
    private void startup() {
        initialiseCache();
    }


    private void initialiseCache() {
        logger.fine("Initialising warp core (cache)..");
        setSimCache(new HashMap<Integer,ResponseBuilder>());
    }

    @Override
    public void print(final String text) {
        logger.fine("Cache is reachable text");
    }

    public HashMap getSimCache() {
        return simCache;
    }

    private void setSimCache(final HashMap simCache) {
        this.simCache = simCache;
    }

    @SuppressWarnings("unchecked")
    public void put(final int key, final ResponseBuilder value) {
        this.simCache.put(key, value);

    }

    public ResponseBuilder get(final int key) {
        return (ResponseBuilder) simCache.get(key);
    }

    public void remove(final String key) {
        this.simCache.remove(key);
    }
}
