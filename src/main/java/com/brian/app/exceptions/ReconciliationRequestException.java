package com.brian.app.exceptions;

public class ReconciliationRequestException extends Exception {

    public ReconciliationRequestException(final String message)
    {
        super(message);
    }

}
