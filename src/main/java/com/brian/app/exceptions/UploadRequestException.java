package com.brian.app.exceptions;

public class UploadRequestException extends Exception {

    public UploadRequestException(final String message)
    {
        super(message);
    }

}
