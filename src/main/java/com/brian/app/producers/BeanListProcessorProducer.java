package com.brian.app.producers;

import com.brian.app.beans.TransactionBean;
import com.univocity.parsers.common.processor.BeanListProcessor;

import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Singleton;

/**
 * Not used
 */
@SuppressWarnings("UnnecessaryLocalVariable")
@Startup
@Singleton
public class BeanListProcessorProducer {

    @Produces
    public BeanListProcessor<TransactionBean> produceBeanListProcessor(final InjectionPoint injectionPoint){
        // BeanListProcessor converts each parsed row to an instance of a given class, then stores each instance into a list.
        final BeanListProcessor<TransactionBean> rowProcessor = new BeanListProcessor<>(TransactionBean.class);
        return rowProcessor;

    }
}
