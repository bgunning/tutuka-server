package com.brian.app.producers;

import com.brian.app.beans.TransactionBean;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import javax.ejb.DependsOn;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Not used -- TODO Improvement
 */
@SuppressWarnings("deprecation")
@Startup
@Singleton
@DependsOn("BeanListProcessorProducer")
public class CSVParserProducer {

    @Inject
    private BeanListProcessor rowProcessor;



    private CsvParserSettings parserSettings;
    @Produces
    public CsvParser produceCSVParser(final InjectionPoint injectionPoint){
        // BeanListProcessor converts each parsed row to an instance of a given class, then stores each instance into a list.
        final BeanListProcessor<TransactionBean> rowProcessor = new BeanListProcessor<>(TransactionBean.class);
        final CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.setRowProcessor(rowProcessor);
        parserSettings.setHeaderExtractionEnabled(true);
        final CsvParser parser = new CsvParser(parserSettings);
        parserSettings.getRowProcessor();
        return parser;

    }

    public CsvParserSettings getParserSettings() {
        return parserSettings;
    }
}
