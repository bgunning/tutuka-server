package com.brian.app.producers;

import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Produce A HashMap for CDI injection
 */
@Startup
@Singleton
public class HashMapProducer {


    private static final Logger logger = Logger.getLogger(LoggerProducer.class.getName());

    /**
     * Produce HashMap
     * @param injectionPoint - the point where the injection is instantiated
     * @return - a HashMap
     */
    @Produces
    public HashMap produceHashMap(final InjectionPoint injectionPoint) {
        final String injectingClass = injectionPoint.getMember().getDeclaringClass().getName();
        logger.fine("creating hashMap for : " + injectingClass);
        return new HashMap<>();
    }

}