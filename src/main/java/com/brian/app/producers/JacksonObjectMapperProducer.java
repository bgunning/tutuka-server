package com.brian.app.producers;



import com.fasterxml.jackson.databind.ObjectMapper;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Singleton;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;

/**
 * Produce an instance of Jackson {@link ObjectMapper} for CDI injection
 */
@Startup
@Singleton
public class JacksonObjectMapperProducer {

    private static final Logger logger = Logger.getLogger(JacksonObjectMapperProducer.class.getName());

    /**
     * Produce a {@link ObjectMapper}
     * @param injectionPoint - the point where injection is called
     * @return - instance of {@link ObjectMapper}
     */
    @Produces
    public ObjectMapper produceObjectMapper(final InjectionPoint injectionPoint) {
        final String injectingClass = injectionPoint.getMember().getDeclaringClass().getName();
        logger.fine("creating Object Mapper for : " + injectingClass);
        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(df);
        return objectMapper;
    }
}
