package com.brian.app.producers;


import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Singleton;
import java.util.logging.Logger;

/**
 * Produce a {@link Logger} for CDI injection
 */
@Startup
@Singleton
public class LoggerProducer {

    private static final Logger logger = Logger.getLogger(LoggerProducer.class.getName());

    /**
     * Produce a {@link Logger} for CDI injection
     * @param injectionPoint - the point where the injection is instantiated
     * @return - a {@link Logger}
     */
    @Produces
    public Logger produceLogger(final InjectionPoint injectionPoint) {
        final String injectingClass = injectionPoint.getMember().getDeclaringClass().getName();
        logger.fine("creating logger for : " + injectingClass);
        return Logger.getLogger(injectingClass);
    }

}
