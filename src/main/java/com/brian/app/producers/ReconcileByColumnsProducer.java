package com.brian.app.producers;


import javax.ejb.Startup;
import javax.inject.Singleton;
import java.util.logging.Logger;

/**
 * Produce a {@link com.brian.app.services.algorithms.ReconcileByColumnMatch} for CDI injection
 */
@Startup
@Singleton
class ReconcileByColumnsProducer {

    private static final Logger logger = Logger.getLogger(ReconcileByColumnsProducer.class.getName());

    /**
     * Produce a {@link com.brian.app.services.algorithms.ReconcileByColumnMatch} for CDI injection
     * @param injectionPoint - the point where the injection is instantiated
     * @return - a {@link com.brian.app.services.algorithms.ReconcileByColumnMatch}
     */
//    @Produces
//    public ReconcileByColumnMatch produceReconcileByColumnMatch(final InjectionPoint injectionPoint) {
//        final String injectingClass = injectionPoint.getMember().getDeclaringClass().getName();
//        logger.fine("creating ReconcileByColumnMatch for : " + injectingClass);
//        return new ReconcileByColumnMatch();
//    }

}
