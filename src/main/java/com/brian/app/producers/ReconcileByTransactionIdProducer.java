package com.brian.app.producers;


import javax.ejb.Startup;
import javax.inject.Singleton;
import java.util.logging.Logger;

/**
 * Produce a {@link com.brian.app.services.algorithms.ReconcileByTransactionId} for CDI injection
 */
@Startup
@Singleton
class ReconcileByTransactionIdProducer {

    private static final Logger logger = Logger.getLogger(ReconcileByTransactionIdProducer.class.getName());

    /**
     * Produce a {@link com.brian.app.services.algorithms.ReconcileByTransactionId} for CDI injection
     * @param injectionPoint - the point where the injection is instantiated
     * @return - a {@link com.brian.app.services.algorithms.ReconcileByTransactionId}
     */
//    @Produces
//    public ReconcileByTransactionId produceReconcileByTransactionId(final InjectionPoint injectionPoint) {
//        final String injectingClass = injectionPoint.getMember().getDeclaringClass().getName();
//        logger.fine("creating ReconcileByTransactionId for : " + injectingClass);
//        return new ReconcileByTransactionId();
//    }

}
