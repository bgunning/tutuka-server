package com.brian.app.producers;


import com.brian.app.services.algorithms.ReconciliationRequest;

import javax.ejb.Startup;
import javax.inject.Singleton;
import java.util.logging.Logger;

/**
 * Produce a {@link ReconciliationRequest} for CDI injection
 */
@Startup
@Singleton
class ReconcileRequestProducer {

    private static final Logger logger = Logger.getLogger(ReconcileRequestProducer.class.getName());

    /**
     * Produce a {@link com.brian.app.services.algorithms.ReconcileByTransactionId} for CDI injection
     * @param injectionPoint - the point where the injection is instantiated
     * @return - a {@link com.brian.app.services.algorithms.ReconcileByTransactionId}
     */
//    @Produces
//    public ReconciliationRequest produceReconciliationRequest(final InjectionPoint injectionPoint) {
//        final String injectingClass = injectionPoint.getMember().getDeclaringClass().getName();
//        logger.fine("creating ReconciliationRequest for : " + injectingClass);
//        return new ReconciliationRequest();
//    }

}
