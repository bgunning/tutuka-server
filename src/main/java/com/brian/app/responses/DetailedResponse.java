package com.brian.app.responses;

import com.brian.app.annotationinterfaces.IDetailedResponse;

import java.util.List;
import java.util.Set;

/**
 * Simple Response Object when user asks for file summary to be reconciled further
 * Contains relevant fields and getters/setter
 */
@IDetailedResponse
public class DetailedResponse {

    private Set potentialMatchesKeys;
    //the same value entry can match multiple keys if fuzzy logic tolerance is low enough
    private List potentialMatchesValues;
    private Set fileOneUnmatched;
    private Set fileTwoUnmatched;
    private Set fileOneDuplicates;
    private Set fileTwoDuplicates;


    public Set getFileOneUnmatched() {
        return fileOneUnmatched;
    }

    public void setFileOneUnmatched(final Set fileOneUnmatched) {
        this.fileOneUnmatched = fileOneUnmatched;
    }

    public Set getFileTwoUnmatched() {
        return fileTwoUnmatched;
    }

    public void setFileTwoUnmatched(final Set fileTwoUnmatched) {
        this.fileTwoUnmatched = fileTwoUnmatched;
    }


    public Set getPotentialMatchesKeys() {
        return potentialMatchesKeys;
    }

    public void setPotentialMatchesKeys(final Set potentialMatchesKeys) {
        this.potentialMatchesKeys = potentialMatchesKeys;
    }

    public List getPotentialMatchesValues() {
        return potentialMatchesValues;
    }

    public void setPotentialMatchesValues(final List potentialMatchesValues) {
        this.potentialMatchesValues = potentialMatchesValues;
    }

    public Set getFileOneDuplicates() {
        return fileOneDuplicates;
    }

    public void setFileOneDuplicates(final Set fileOneDuplicates) {
        this.fileOneDuplicates = fileOneDuplicates;
    }

    public Set getFileTwoDuplicates() {
        return fileTwoDuplicates;
    }

    public void setFileTwoDuplicates(final Set fileTwoDuplicates) {
        this.fileTwoDuplicates = fileTwoDuplicates;
    }

    @Override
    public String toString() {
        return "DetailedResponse{" +
                "potentialMatchesKeys=" + potentialMatchesKeys +
                ", potentialMatchesValues=" + potentialMatchesValues +
                ", fileOneUnmatched=" + fileOneUnmatched +
                ", fileTwoUnmatched=" + fileTwoUnmatched +
                ", fileOneDuplicates=" + fileOneDuplicates +
                ", fileTwoDuplicates=" + fileTwoDuplicates +
                '}';
    }
}
