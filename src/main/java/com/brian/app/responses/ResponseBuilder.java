package com.brian.app.responses;

import com.brian.app.beans.TransactionBean;
import com.brian.app.annotationinterfaces.IResponseBuilder;
import com.brian.app.utilities.StringConstants;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Used to build Summary Response to send back to user after file comparison and object is stored
 * in {@link com.brian.app.cache.SimpleCache} for retrieval when generating potential matches
 */
@IResponseBuilder
public class ResponseBuilder implements Serializable {

    private List<TransactionBean> fileOneBeans;
    private List<TransactionBean> fileTwoBeans;
    private Set<TransactionBean>  fileOneDuplicates;
    private Set<TransactionBean>  fileTwoDuplicates;
    private Set<TransactionBean>  fileOneUnmatched;
    private Set<TransactionBean>  fileTwoUnmatched;

     /**
     * Set file name
     * @param fileKey - the key used in upload, file1 or file2
     * @param fileName - the actual file name
     * @param response - the {@link SummaryResponse} to be sent back to the user
     */
    public void setFileName(final String fileKey, final String fileName, final SummaryResponse response) {

        switch (fileKey) {
            case StringConstants.FILE_ONE:
                response.setFileOneName(fileName);
                break;
            case StringConstants.FILE_TWO:
                response.setFileTwoName(fileName);
                break;
        }
    }

    /**
     *
     * @param fileKey  - the key used in upload, file1 or file2
     * @param fileTotal - total number of transactions in the file
     * @param response - the {@link SummaryResponse} to be sent back to the userr
     */
    public void setTotal(final String fileKey, final int fileTotal, final SummaryResponse response) {

        switch (fileKey) {
            case StringConstants.FILE_ONE:
                response.setFileOneTotal(fileTotal);
                break;
            case StringConstants.FILE_TWO:
                response.setFileTwoTotal(fileTotal);
                break;
        }
    }

    /**
     * Set number of matching transactions
     * @param matching - the number of matching transactions
     * @param response - the {@link SummaryResponse} to be sent back to the user
     */
    public void setMatching(final int matching, final SummaryResponse response) {
        //same value for both files
        response.setFileOneMatching(matching);
        response.setFileTwoMatching(matching);

    }

    /**
     * Set number of unmatched transactions
     * @param fileKey - the key used in upload, file1 or file2
     * @param unmatched - the number of unmatched transactions
     * @param response - the {@link SummaryResponse} to be sent back to the user
     */
    public void setUnmatched(final String fileKey, final int unmatched, final SummaryResponse response) {

        switch (fileKey) {
            case StringConstants.FILE_ONE:
                response.setFileOneUnmatched(unmatched);
                break;
            case StringConstants.FILE_TWO:
                response.setFileTwoUnmatched(unmatched);
                break;
        }
    }

    /**
     * Set the number of duplicate transactions in a file
     * @param fileKey - the key used in upload, file1 or file2
     * @param duplicates - the number of duplicates
     * @param response - the {@link SummaryResponse} to be sent back to the user
     */
    public void setDuplicates(final String fileKey, final int duplicates, final SummaryResponse response) {

        switch (fileKey) {
            case StringConstants.FILE_ONE:
                response.setFileOneDuplicates(duplicates);
                break;
            case StringConstants.FILE_TWO:
                response.setFileTwoDuplicates(duplicates);
                break;
        }
    }


    /**
     * Store the treansaction beans for retrieval when generating the detailed report
     * @param fileKey - the key used in upload, file1 or file2
     * @param fileBeans - the transactions in a file
     */
    public void setFileBeans(final String fileKey, final List<TransactionBean> fileBeans) {

        switch (fileKey) {
            case StringConstants.FILE_ONE:
                this.fileOneBeans = fileBeans;
                break;
            case StringConstants.FILE_TWO:
                this.fileTwoBeans = fileBeans;
                break;
        }
    }

    public List<TransactionBean> getFileTwoBeans() {
        return fileTwoBeans;
    }

    public List<TransactionBean> getFileOneBeans() {
        return fileOneBeans;
    }

    public Set<TransactionBean> getFileOneDuplicates() {
        return fileOneDuplicates;
    }

    public void setFileOneDuplicates(final Set<TransactionBean> fileOneDuplicates) {
        this.fileOneDuplicates = fileOneDuplicates;
    }

    public Set<TransactionBean> getFileTwoDuplicates() {
        return fileTwoDuplicates;
    }

    public void setFileTwoDuplicates(final Set<TransactionBean> fileTwoDuplicates) {
        this.fileTwoDuplicates = fileTwoDuplicates;
    }

    public Set<TransactionBean> getFileOneUnmatched() {
        return fileOneUnmatched;
    }

    public void setFileOneUnmatched(final Set<TransactionBean> fileOneUnmatched) {
        this.fileOneUnmatched = fileOneUnmatched;
    }

    public Set<TransactionBean> getFileTwoUnmatched() {
        return fileTwoUnmatched;
    }

    public void setFileTwoUnmatched(final Set<TransactionBean> fileTwoUnmatched) {
        this.fileTwoUnmatched = fileTwoUnmatched;
    }

}
