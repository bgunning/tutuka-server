package com.brian.app.responses;

import com.brian.app.annotationinterfaces.ISummaryResponse;

/**
 * Response sent when two files are compared
 */
@ISummaryResponse
public class SummaryResponse {

    private  int id=0;
    private  String fileOneName=null;
    private  int fileOneTotal=0;
    private  int fileOneMatching=0;
    private  int fileOneUnmatched=0;
    private  int fileOneDuplicates=0;
    private  String fileTwoName=null;
    private  int fileTwoTotal=0;
    private  int fileTwoMatching=0;
    private  int fileTwoUnmatched=0;
    private  int fileTwoDuplicates=0;

    public SummaryResponse() {
    }

    public String getFileOneName() {
        return fileOneName;
    }

    public void setFileOneName(final String fileOneName) {
        this.fileOneName = fileOneName;
    }

    public int getFileOneTotal() {
        return fileOneTotal;
    }

    public void setFileOneTotal(final int fileOneTotal) {
        this.fileOneTotal = fileOneTotal;
    }

    public int getFileOneMatching() {
        return fileOneMatching;
    }

    public void setFileOneMatching(final int fileOneMatching) {
        this.fileOneMatching = fileOneMatching;
    }

    public int getFileOneUnmatched() {
        return fileOneUnmatched;
    }

    public void setFileOneUnmatched(final int fileOneUnmatched) {
        this.fileOneUnmatched = fileOneUnmatched;
    }

    public String getFileTwoName() {
        return fileTwoName;
    }

    public void setFileTwoName(final String fileTwoName) {
        this.fileTwoName = fileTwoName;
    }

    public int getFileTwoTotal() {
        return fileTwoTotal;
    }

    public void setFileTwoTotal(final int fileTwoTotal) {
        this.fileTwoTotal = fileTwoTotal;
    }

    public int getFileTwoMatching() {
        return fileTwoMatching;
    }

    public void setFileTwoMatching(final int fileTwoMatching) {
        this.fileTwoMatching = fileTwoMatching;
    }

    public int getFileTwoUnmatched() {
        return fileTwoUnmatched;
    }

    public void setFileTwoUnmatched(final int fileTwoUnmatched) {
        this.fileTwoUnmatched = fileTwoUnmatched;
    }

    public int getFileOneDuplicates() {
        return fileOneDuplicates;
    }

    public void setFileOneDuplicates(final int fileOneDuplicates) {
        this.fileOneDuplicates = fileOneDuplicates;
    }

    public int getFileTwoDuplicates() {
        return fileTwoDuplicates;
    }

    public void setFileTwoDuplicates(final int fileTwoDuplicates) {
        this.fileTwoDuplicates = fileTwoDuplicates;
    }


    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SummaryResponse{" +
                "id=" + id +
                ", fileOneName='" + fileOneName + '\'' +
                ", fileOneTotal=" + fileOneTotal +
                ", fileOneMatching=" + fileOneMatching +
                ", fileOneUnmatched=" + fileOneUnmatched +
                ", fileOneDuplicates=" + fileOneDuplicates +
                ", fileTwoName='" + fileTwoName + '\'' +
                ", fileTwoTotal=" + fileTwoTotal +
                ", fileTwoMatching=" + fileTwoMatching +
                ", fileTwoUnmatched=" + fileTwoUnmatched +
                ", fileTwoDuplicates=" + fileTwoDuplicates +
                '}';
    }
}
