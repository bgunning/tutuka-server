package com.brian.app.services;


import com.brian.app.annotationinterfaces.IAppUtilities;
import com.brian.app.annotationinterfaces.IFileReconciler;
import com.brian.app.annotationinterfaces.FileHandler;
import com.brian.app.services.algorithms.ReconciliationRequest;
import com.brian.app.services.algorithms.ReconciliationStrategyEnum;
import com.brian.app.services.handlers.ReconcileFileHandler;
import com.brian.app.services.handlers.UploadedFileHandler;
import com.brian.app.utilities.AppUtilities;
import com.brian.app.utilities.StringConstants;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;


/**
 * HTTP endpoint for File comparison services. Exposes endpoints to upload two files for comparison
 * and further reconciliation/processing of the result
 */
@Path("/v1/reconcile")
public class FileComparisonService {

    @Inject
    private Logger logger;

    @Inject
    @FileHandler
    private UploadedFileHandler fileHandler;

    @Inject
    @IFileReconciler
    private ReconcileFileHandler reconciliationHandler;

    @Inject
    @IAppUtilities
    private AppUtilities appUtilities;

    @Inject
    private ReconciliationRequest reconciliationRequest;


    /**
     * Generate potential matches for umatched records
     *
     * @param id - the id of the file comparison request
     * @return - suggested matches, unmatched transactions and duplicate entries
     */
    @GET
    @Path("/column/{id}")
    public Response getMatches(@PathParam("id") final String id) {
        logger.fine("Retrieving transaction matches");
        try {
            final int reconciliationId = Integer.valueOf(id);
            reconciliationRequest.setRecordId(reconciliationId);
            reconciliationRequest.setStrategy(ReconciliationStrategyEnum.BASIC);
            final String body;
            body = reconciliationHandler.reconcileFiles(reconciliationRequest);
            return Response.status(200).entity(body).build();
        } catch (final NumberFormatException e) {
            final String msg = appUtilities.buildErrorResponseFromString(StringConstants.UNABLE_TO_LOCATE_RECORD);
            return Response.status(400).entity(msg).build();
        } catch (final Exception e) {
            final String msg = appUtilities.buildErrorResponseFromString(e.getMessage());
            return Response.status(400).entity(msg).build();
        }

    }

    /**
     * Generate potential matches for unmatched records
     *
     * @param id - the id of the file comparison request
     * @return - suggested matches, unmatched transactions and duplicate entries
     */
    @GET
    @Path("/fuzzy/{id}/{fuzzyRatio}")
    public Response getMatchesFuzzy(@PathParam("id") final String id, @PathParam("fuzzyRatio") final int fuzzyRatio) {
        logger.fine("Retrieving transaction matches");
        try {
            final int reconciliationId = Integer.valueOf(id);
            reconciliationRequest.setRecordId(reconciliationId);
            reconciliationRequest.setFuzzyTolerance(fuzzyRatio);
            reconciliationRequest.setStrategy(ReconciliationStrategyEnum.FUZZY_LOGIC);
            final String body;
            body = reconciliationHandler.reconcileFiles(reconciliationRequest);
            return Response.status(200).entity(body).build();
        } catch (final NumberFormatException e) {
            final String msg = appUtilities.buildErrorResponseFromString(StringConstants.UNABLE_TO_LOCATE_RECORD);
            return Response.status(400).entity(msg).build();
        } catch (final Exception e) {
            final String msg = appUtilities.buildErrorResponseFromString(e.getMessage());
            return Response.status(400).entity(msg).build();
        }

    }

    /**
     * Generate potential matches for unmatched records
     *
     * @param id - the id of the file comparison request
     * @return - suggested matches, unmatched transactions and duplicate entries
     */
    @GET
    @Path("/transaction/{id}")
    public Response getMatchesByTransactionId(@PathParam("id") final String id) {
        logger.fine("Retrieving transaction matches");
        try {
            final int reconciliationId = Integer.valueOf(id);
            final String body;
            reconciliationRequest.setStrategy(ReconciliationStrategyEnum.TRANSACTION_ID);
            reconciliationRequest.setRecordId(reconciliationId);
            body = reconciliationHandler.reconcileFiles(reconciliationRequest);
            return Response.status(200).entity(body).build();
        } catch (final NumberFormatException e) {
            final String msg = appUtilities.buildErrorResponseFromString(StringConstants.UNABLE_TO_LOCATE_RECORD);
            return Response.status(400).entity(msg).build();
        } catch (final Exception e) {
            final String msg = appUtilities.buildErrorResponseFromString(e.getMessage());
            return Response.status(400).entity(msg).build();
        }

    }


}
