package com.brian.app.services;


import com.brian.app.annotationinterfaces.FileHandler;
import com.brian.app.annotationinterfaces.IAppUtilities;
import com.brian.app.annotationinterfaces.IFileReconciler;
import com.brian.app.exceptions.UploadRequestException;
import com.brian.app.services.handlers.ReconcileFileHandler;
import com.brian.app.services.handlers.UploadedFileHandler;
import com.brian.app.utilities.AppUtilities;
import com.brian.app.utilities.StringConstants;
import com.univocity.parsers.common.DataProcessingException;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.logging.Logger;


/**
 * HTTP endpoint for File Comarison services. Exposes endpoints to upload two files for comparison
 * and furthr reconciliation/processing of the result
 */
@Path("/v1/upload")
public class FileUploadService {

    @Inject
    private Logger logger;

    @Inject
    @FileHandler
    private UploadedFileHandler fileHandler;

    @Inject
    @IFileReconciler
    private ReconcileFileHandler reconciliationHandler;

    @Inject
    @IAppUtilities
    private AppUtilities appUtilities;


    /**
     * Handle uploaded files. Restrictions around accepted file size etc. should be handled at web server tier
     *
     * @param input - The multi path form data
     * @return - The file comparison summary {@link com.brian.app.responses.SummaryResponse}
     */
    @POST
    @Consumes({"multipart/form-data"})
    @Path("/files")
    public Response uploadFile(final MultipartFormDataInput input) {
        logger.info("Handling uploaded files");
        final String response;
        final String errorMessage;
        try {

            response = fileHandler.handleFile(input);
            return Response.status(200)
                    .entity(response).build();

        } catch (final IOException | NullPointerException | DataProcessingException  e  ) {
            logger.severe(e.getMessage());
            errorMessage = appUtilities.buildErrorResponseFromString(StringConstants.DATA_PROCESSING_ERROR);
            return Response.status(400).entity(errorMessage).build();
        }
        catch(final UploadRequestException e){
            logger.severe(e.getMessage());
            errorMessage = appUtilities.buildErrorResponseFromString(e.getMessage());
            return Response.status(400).entity(errorMessage).build();
        }
        catch (final Exception e) {
            logger.severe(e.getMessage());
            errorMessage = appUtilities.buildErrorResponseFromString(StringConstants.UNKNOWN_ERROR);
            return Response.status(400).entity(errorMessage).build();
        }

    }


}
