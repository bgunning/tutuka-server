package com.brian.app.services;


import com.brian.app.utilities.StringConstants;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.logging.Logger;

import static java.util.Calendar.getInstance;


/**
 * HTTP endpoint for File Comarison services. Exposes endpoints to upload two files for comparison
 * and furthr reconciliation/processing of the result
 */
@Path("/v1/health")
public class SystemHealthService {

    @Inject
    private Logger logger;


    /**
     * Simple healthcheck
     * @return - HTTP 200 and current server time
     */
    @GET
    @Path("/simple")
    public Response health() {
        logger.finer("in health");
        final Date serverTime = getInstance().getTime();
        return Response.status(200).entity(StringConstants.HEALTH_JSON_FORMAT + StringConstants.SERVERTIME_JSON_FORMAT + serverTime + StringConstants.JSON_CLOSE_BRACKET).build();
    }




}
