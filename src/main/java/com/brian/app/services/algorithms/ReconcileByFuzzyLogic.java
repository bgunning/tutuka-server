package com.brian.app.services.algorithms;

import com.brian.app.annotationinterfaces.IAppUtilities;
import com.brian.app.annotationinterfaces.IDetailedResponse;
import com.brian.app.annotationinterfaces.Simple;
import com.brian.app.beans.TransactionBean;
import com.brian.app.cache.SimpleCache;
import com.brian.app.exceptions.ReconciliationRequestException;
import com.brian.app.responses.DetailedResponse;
import com.brian.app.responses.ResponseBuilder;
import com.brian.app.utilities.AppUtilities;
import com.brian.app.utilities.StringConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import me.xdrop.fuzzywuzzy.FuzzySearch;

import javax.inject.Inject;
import java.util.*;
import java.util.logging.Logger;

public class ReconcileByFuzzyLogic implements ReconcileFiles {
    @Inject
    private
    Logger logger;
    @Inject
    @IDetailedResponse
    private DetailedResponse detailedRespose;
    @Inject
    @Simple
    private SimpleCache simpleCache;
    @Inject
    @IAppUtilities
    private AppUtilities appUtils;

    @Override
    public String reconcile(final ReconciliationRequest reconciliationRequest) throws ReconciliationRequestException {
        try {
            return reconcileFilesFuzzy(reconciliationRequest.getRecordId(),reconciliationRequest.getFuzzyTolerance());
        } catch (final ReconciliationRequestException e) {
            throw new ReconciliationRequestException(StringConstants.UNABLE_TO_LOCATE_RECORD);
        }  catch (final JsonProcessingException e) {
        throw new ReconciliationRequestException(StringConstants.ERROR_PROCESSING_JSON_MESSAGE + " " +e.getMessage());
    }
    }


    /**
     * Retrieve File Reconciliation record from the cache for processing
     *
     * @param reconciliationId - the record ID
     * @param fzzyPercentage - the percentage match level for fuzzy logic
     * @return - Potential matches, unmatched records and duplicate entries
     * @throws Exception - bespoke exception if record not found
     */
    private String reconcileFilesFuzzy(final int reconciliationId, final int fzzyPercentage) throws ReconciliationRequestException, JsonProcessingException {
        logger.info("Comparing records for fileReconciliationRecord:  " + reconciliationId);
        final ResponseBuilder fileReconciliationRecord = simpleCache.get(reconciliationId);

        if (fileReconciliationRecord != null) {
            return compareUnmatchedTransactions(fileReconciliationRecord, fzzyPercentage);

        } else {
            logger.severe(StringConstants.UNABLE_TO_LOCATE_RECORD + ": " + reconciliationId);
            throw new ReconciliationRequestException(StringConstants.UNABLE_TO_LOCATE_RECORD);
        }
    }



    /**
     * Compare unmatched records from two files and propose matches based on fuzzy logic algorithm
     * This implementation uses Levenshtein distance to calculate similarity between strings.
     * It uses the java implementation of a python library ref: https://github.com/xdrop/fuzzywuzzy
     *
     * @param fileReconciliationRecord - the file reconciliaion id
     * @param fzzyPercentage
     * @return - Potential matches, unmatched records and duplicate entries
     * @throws JsonProcessingException - Json exception
     */
    @SuppressWarnings("unchecked")
    private String compareUnmatchedTransactions(final ResponseBuilder fileReconciliationRecord, final int fzzyPercentage) throws JsonProcessingException {

        final LinkedHashSet potentialMatchesKeySet = new LinkedHashSet();
        final LinkedList potentialMatchesValues = new LinkedList<>();
        final Set<TransactionBean> fileOneUnmatchedSet = fileReconciliationRecord.getFileOneUnmatched();
        final Set<TransactionBean> fileTwoUnmatchedSet = fileReconciliationRecord.getFileTwoUnmatched();
        //make copies to allow modification
        final Set<TransactionBean> fileOneUnmatchedSetCopy = new HashSet<>(fileOneUnmatchedSet);
        final Set<TransactionBean> fileTwoUnmatchedSetCopy = new HashSet<>(fileTwoUnmatchedSet);

        final HashMap<TransactionBean, TransactionBean> potentialMatchesMap = new HashMap<>();


        //extract out to separate method
        int fuzzyComparison;
        TransactionBean txKey = null;
        TransactionBean txValue = null;
            for (final TransactionBean tx1 : fileOneUnmatchedSetCopy) {

                int lastMatchedValue = 0;
                for (final TransactionBean tx2 : fileTwoUnmatchedSetCopy) {
                    fuzzyComparison = FuzzySearch.ratio(tx1.toSanatizedString(), tx2.toSanatizedString());
                    //find highest match
                    if (fuzzyComparison > lastMatchedValue) {
                        logger.fine("fuzzyComparison greater: "+fuzzyComparison+"  : lastMatchedValue: "+lastMatchedValue);
                        lastMatchedValue = fuzzyComparison;
                        txKey = tx1;
                        txValue = tx2;
                    }
                }

                if (lastMatchedValue > fzzyPercentage) {
                    logger.fine("Potential Match found: \n TX1: " + txKey + "\nTX2: " + txValue);
                    potentialMatchesMap.put(txKey, txValue);
                    //Using two separate ordered sets as Jackson Object Mapper
                    //is corrupting the HashMap when it converts it to JSON
                    if(potentialMatchesKeySet.contains(txKey)){
                        logger.severe("ERROR contains key ..on.!");
                    }
                    if(potentialMatchesKeySet.contains(txValue)){
                        logger.severe("ERROR contains value..!");
                    }
                    potentialMatchesKeySet.add(txKey);
                    potentialMatchesValues.add(txValue);

                } else {
                    logger.finest("No MATCH: " + tx1);
                }

            }

        fileOneUnmatchedSetCopy.removeAll(potentialMatchesMap.keySet());
        fileTwoUnmatchedSetCopy.removeAll(potentialMatchesMap.values());
        logger.fine("trimmed exclusiveToFile1 size: " + fileOneUnmatchedSetCopy.size());
        logger.fine("trimmed exclusiveToFile2 size: " + fileTwoUnmatchedSetCopy.size());

        logger.fine("trimmed exclusiveToFile1: " + fileOneUnmatchedSetCopy);
        logger.fine("trimmed exclusiveToFile2: " + fileTwoUnmatchedSetCopy);

        detailedRespose.setPotentialMatchesKeys(potentialMatchesKeySet);
        detailedRespose.setPotentialMatchesValues(potentialMatchesValues);
        detailedRespose.setFileOneUnmatched(fileOneUnmatchedSetCopy);
        detailedRespose.setFileTwoUnmatched(fileTwoUnmatchedSetCopy);
        detailedRespose.setFileOneDuplicates(fileReconciliationRecord.getFileOneDuplicates());
        detailedRespose.setFileTwoDuplicates(fileReconciliationRecord.getFileTwoDuplicates());

        logger.info("Detailed Response: " + detailedRespose);
        return appUtils.convertViaJacksonToJson(detailedRespose);

    }

}
