package com.brian.app.services.algorithms;

import com.brian.app.annotationinterfaces.IAppUtilities;
import com.brian.app.annotationinterfaces.IDetailedResponse;
import com.brian.app.annotationinterfaces.Simple;
import com.brian.app.beans.TransactionBean;
import com.brian.app.cache.SimpleCache;
import com.brian.app.exceptions.ReconciliationRequestException;
import com.brian.app.responses.DetailedResponse;
import com.brian.app.responses.ResponseBuilder;
import com.brian.app.utilities.AppUtilities;
import com.brian.app.utilities.StringConstants;
import com.fasterxml.jackson.core.JsonProcessingException;

import javax.inject.Inject;
import java.util.*;
import java.util.logging.Logger;

public class ReconcileByTransactionId implements ReconcileFiles {

    @Inject
    private
    Logger logger;
    @Inject
    @IDetailedResponse
    private DetailedResponse detailedRespose;
    @Inject
    @Simple
    private SimpleCache simpleCache;

    @Inject
    @IAppUtilities
    private AppUtilities appUtils;

    /**
     * Retrieve File Reconciliation record from the cache for processing
     * @param reconciliationRequest -
     * @return Potential matches, unmatched records and duplicate entries
     * @throws ReconciliationRequestException
     */
    @Override
    public String reconcile(final ReconciliationRequest reconciliationRequest) throws ReconciliationRequestException {
        try {
            return reconcileFilesByTransactionId(reconciliationRequest.getRecordId());
        } catch (final ReconciliationRequestException e) {
            throw new ReconciliationRequestException(StringConstants.UNABLE_TO_LOCATE_RECORD);
        } catch (final JsonProcessingException e) {
            throw new ReconciliationRequestException(StringConstants.ERROR_PROCESSING_JSON_MESSAGE + " " +e.getMessage());
        }
    }


    private String reconcileFilesByTransactionId(final int reconciliationId) throws ReconciliationRequestException, JsonProcessingException {
        logger.info("Comparing records for fileReconciliationRecord:  " + reconciliationId);
        final ResponseBuilder fileReconciliationRecord = simpleCache.get(reconciliationId);

        if (fileReconciliationRecord != null) {
            return compareUnmatchedTransactions(fileReconciliationRecord);

        } else {
            logger.severe(StringConstants.UNABLE_TO_LOCATE_RECORD + ": " + reconciliationId);
            throw new ReconciliationRequestException(StringConstants.UNABLE_TO_LOCATE_RECORD);
        }
    }


    /**
     * Comapare unmatched records from two files and propose matches based on matching transaction id
     *
     * @param fileReconciliationRecord - the file reconciliaion id
     * @return - Potential matches, unmatched records and duplicate entries
     * @throws JsonProcessingException - Json exception
     */
    @SuppressWarnings("unchecked")
    private String compareUnmatchedTransactions(final ResponseBuilder fileReconciliationRecord) throws JsonProcessingException {

        final LinkedHashSet potentialMatchesKeySet = new LinkedHashSet();
        final LinkedList potentialMatchesValues = new LinkedList<>();
        final Set<TransactionBean> fileOneUnmatchedSet = fileReconciliationRecord.getFileOneUnmatched();
        final Set<TransactionBean> fileTwoUnmatchedSet = fileReconciliationRecord.getFileTwoUnmatched();
        //make copies to allow modification
        final Set<TransactionBean> fileOneUnmatchedSetCopy = new HashSet<>(fileOneUnmatchedSet);
        final Set<TransactionBean> fileTwoUnmatchedSetCopy = new HashSet<>(fileTwoUnmatchedSet);

        final HashMap<TransactionBean, TransactionBean> potentialMatchesMap = new HashMap<>();

        for (final TransactionBean tx1 : fileOneUnmatchedSetCopy) {
            for (final TransactionBean tx2 : fileTwoUnmatchedSetCopy) {
                if (tx1.getTransactionID()==tx2.getTransactionID()) {
                    logger.fine("Potential Match found: \n TX1: " + tx1 + "\nTX2: " + tx2);
                    potentialMatchesMap.put(tx1, tx2);
                    //Using two separate ordered sets as Jackson Object Mapper
                    //is corrupting the HashMap when it converts it to JSON
                    potentialMatchesKeySet.add(tx1);
                    potentialMatchesValues.add(tx2);

                } else {
                    logger.finest("No MATCH: " + tx1);
                }
            }
        }



        fileOneUnmatchedSetCopy.removeAll(potentialMatchesMap.keySet());
        fileTwoUnmatchedSetCopy.removeAll(potentialMatchesMap.values());
        logger.fine("trimmed exclusiveToFile1 size: " + fileOneUnmatchedSetCopy.size());
        logger.fine("trimmed exclusiveToFile2 size: " + fileTwoUnmatchedSetCopy.size());

        logger.fine("trimmed exclusiveToFile1: " + fileOneUnmatchedSetCopy);
        logger.fine("trimmed exclusiveToFile2: " + fileTwoUnmatchedSetCopy);

        detailedRespose.setPotentialMatchesKeys(potentialMatchesKeySet);
        detailedRespose.setPotentialMatchesValues(potentialMatchesValues);
        detailedRespose.setFileOneUnmatched(fileOneUnmatchedSetCopy);
        detailedRespose.setFileTwoUnmatched(fileTwoUnmatchedSetCopy);
        detailedRespose.setFileOneDuplicates(fileReconciliationRecord.getFileOneDuplicates());
        detailedRespose.setFileTwoDuplicates(fileReconciliationRecord.getFileTwoDuplicates());

        logger.info("Detailed Response: " + detailedRespose);
        return appUtils.convertViaJacksonToJson(detailedRespose);

    }

}
