package com.brian.app.services.algorithms;

import com.brian.app.exceptions.ReconciliationRequestException;


public interface ReconcileFiles {

    String reconcile(ReconciliationRequest reconciliationRequest) throws ReconciliationRequestException;

}
