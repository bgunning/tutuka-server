package com.brian.app.services.algorithms;

/**
 * Class to hold required information for a reconciliation request
 */
public class ReconciliationRequest {

    private int recordId;
    private int fuzzyTolerance;
    private int transactionId;
    private ReconciliationStrategyEnum strategy;

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(final int recordId) {
        this.recordId = recordId;
    }

    public int getFuzzyTolerance() {
        return fuzzyTolerance;
    }

    public void setFuzzyTolerance(final int fuzzyTolerance) {
        this.fuzzyTolerance = fuzzyTolerance;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(final int transactionId) {
        this.transactionId = transactionId;
    }

    public ReconciliationStrategyEnum getStrategy() {
        return strategy;
    }

    public void setStrategy(final ReconciliationStrategyEnum strategy) {
        this.strategy = strategy;
    }
}
