package com.brian.app.services.algorithms;

public enum ReconciliationStrategyEnum {

    BASIC(0),
    FUZZY_LOGIC(1),
    TRANSACTION_ID(2);

    private final int strategy;

    ReconciliationStrategyEnum(final int strategy) {
        this.strategy = strategy;
    }

    /**
     * @return  the strategy
     */
    public int getStrategy() {
        return strategy;
    }

}