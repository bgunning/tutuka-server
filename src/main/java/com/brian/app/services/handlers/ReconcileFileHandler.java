package com.brian.app.services.handlers;

import com.brian.app.annotationinterfaces.*;
import com.brian.app.exceptions.ReconciliationRequestException;
import com.brian.app.services.algorithms.*;

import javax.inject.Inject;
import java.util.logging.Logger;

/**
 * Choose reconciliation strategy
 */
@IFileReconciler
public class ReconcileFileHandler {

    @Inject
    private Logger logger;

    @Inject
    private ReconcileByTransactionId reconcileByTransactionId;
    
    @Inject
    private ReconcileByColumnMatch reconcileByColumnMatch;
    
    @Inject
    private ReconcileByFuzzyLogic reconcileByFuzzyLogic;

    public String reconcileFiles(final ReconciliationRequest reconciliationRequest) throws ReconciliationRequestException {

        switch(reconciliationRequest.getStrategy()){
            case FUZZY_LOGIC:
                logger.fine(ReconciliationStrategyEnum.FUZZY_LOGIC.name()+" detected ");
                return reconcileByFuzzyLogic.reconcile(reconciliationRequest);
            case TRANSACTION_ID:
                logger.fine(ReconciliationStrategyEnum.TRANSACTION_ID.name()+" detected ");
                return reconcileByTransactionId.reconcile(reconciliationRequest);
            case BASIC:
                logger.fine(ReconciliationStrategyEnum.BASIC.name()+" detected ");
                return reconcileByColumnMatch.reconcile(reconciliationRequest);
            default:
                logger.info("Unable to detect strategy, using: "+ReconciliationStrategyEnum.BASIC.name());
                return reconcileByColumnMatch.reconcile(reconciliationRequest);
        }
    }
}
