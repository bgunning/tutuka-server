package com.brian.app.services.handlers;


import com.brian.app.annotationinterfaces.*;
import com.brian.app.exceptions.UploadRequestException;
import com.brian.app.responses.ResponseBuilder;
import com.brian.app.responses.SummaryResponse;
import com.brian.app.beans.TransactionBean;
import com.brian.app.cache.*;
import com.brian.app.utilities.AppUtilities;
import com.brian.app.utilities.StringConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.univocity.parsers.common.DataProcessingException;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.apache.commons.lang.math.RandomUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.logging.Logger;

@SuppressWarnings("deprecation")
@FileHandler
public class UploadedFileHandler {

    private static final String BACK_SLASH = "\"";
    @Inject @Simple
    private SimpleCache simpleCache;
    @Inject
    private Logger logger;
    @SuppressWarnings("CdiInjectionPointsInspection")
    @Inject
    private ObjectMapper objMapper;
    @Inject @ISummaryResponse
    private SummaryResponse summaryResponse;
    @Inject @IResponseBuilder
    private ResponseBuilder summaryResponseBuilder;
    @Inject @IAppUtilities
    private AppUtilities appUtils;


    /**
     * Parse InputParts from the file and send on to {@link #compareTransactions(ResponseBuilder, SummaryResponse)} for comparison
     * @param input - The uploaded files
     * @return - Summary result of the comparison using {@link SummaryResponse}
     */
    public String handleFile(final MultipartFormDataInput input) throws IOException, DataProcessingException, UploadRequestException {

        logger.fine("in handleFile");
        final Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
                    final List<InputPart> fileOneInputParts = uploadForm.get(StringConstants.FILE_ONE);
        final List<InputPart> fileTwoInputParts = uploadForm.get(StringConstants.FILE_TWO);

        if(fileOneInputParts == null || fileTwoInputParts == null)
            throw new NullPointerException("Unable to parse file");

        processInputParts(fileOneInputParts, StringConstants.FILE_ONE);
        processInputParts(fileTwoInputParts, StringConstants.FILE_TWO);
        final SummaryResponse fileComparisonResult = compareTransactions(summaryResponseBuilder, summaryResponse);


        return appUtils.convertViaJacksonToJson(fileComparisonResult);

    }

    /**
     * @param inputParts - inputParts from the file
     * @param theFileKey - file1 or file2
     */
    private void processInputParts(final List<InputPart> inputParts, final String theFileKey) throws UploadRequestException, DataProcessingException, IOException {

        List<TransactionBean> fileBeans;
        String fileName;
        for (final InputPart inputPart : inputParts) {
                final MultivaluedMap<String, String> header = inputPart.getHeaders();
                appUtils.isCsvFile(header);
                fileName = getFileName(header);
                //convert the uploaded file to inputstream
                final InputStream inputStream = inputPart.getBody(InputStream.class, null);
                fileBeans = processInputStreamWithUnivocity(inputStream);
                logger.fine("Input stream processed for: "+fileName);

            summaryResponseBuilder.setFileName(theFileKey, fileName, summaryResponse);
            summaryResponseBuilder.setFileBeans(theFileKey, fileBeans);
            summaryResponseBuilder.setTotal(theFileKey, fileBeans != null ? fileBeans.size() : 0, summaryResponse);

        }

    }

    /**
     * Process the inputstream
     * @param inputStream - the input stream
     * @return - all transactions in a list of TransactionBean
     */
    private List<TransactionBean> processInputStreamWithUnivocity(final InputStream inputStream) throws DataProcessingException{
        final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        // BeanListProcessor converts each parsed row to an instance of a given class, then stores each instance into a list.
        final BeanListProcessor<TransactionBean> rowProcessor = new BeanListProcessor<>(TransactionBean.class);
        final CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.setRowProcessor(rowProcessor);
        parserSettings.setHeaderExtractionEnabled(true);
        final CsvParser parser = new CsvParser(parserSettings);
        parser.parse(reader);

        // The BeanListProcessor provides a list of objects extracted from the input.
        final List<TransactionBean> beans = rowProcessor.getBeans();
        for (final TransactionBean tx : beans) {
            logger.fine("Tx: " + tx.toString());
        }

        return beans;

    }

    /**
     * Compare the transactions and build response
     * @param responseBuilder - builder object for summary respnse
     * @param response - the response
     * @return - Summary Response
     */
    private SummaryResponse compareTransactions(final ResponseBuilder responseBuilder, final SummaryResponse response) {

        final Set<TransactionBean> file1duplicates = removeDuplicates(responseBuilder.getFileOneBeans());
        final Set<TransactionBean> file2duplicates = removeDuplicates(responseBuilder.getFileTwoBeans());
        response.setFileOneDuplicates(file1duplicates.size());
        response.setFileTwoDuplicates(file2duplicates.size());
        responseBuilder.setFileOneDuplicates(file1duplicates);
        responseBuilder.setFileTwoDuplicates(file2duplicates);

        final Set<TransactionBean> matchingTransactions = new LinkedHashSet<>(responseBuilder.getFileOneBeans());
        final Set<TransactionBean> exclusiveToFile1 = new LinkedHashSet<>(responseBuilder.getFileOneBeans());
        final Set<TransactionBean> exclusiveToFile2 = new LinkedHashSet<>(responseBuilder.getFileTwoBeans());

        matchingTransactions.retainAll(exclusiveToFile2);
        exclusiveToFile1.removeAll(matchingTransactions);
        exclusiveToFile2.removeAll(matchingTransactions);

        logger.finer("matchingTransactions: " + matchingTransactions.size());
        logger.finer("exclusiveToFile1: " + exclusiveToFile1.size());
        logger.finer("exclusiveToFile2: " + exclusiveToFile2.size());

        responseBuilder.setMatching(matchingTransactions.size(), summaryResponse);
        responseBuilder.setUnmatched(StringConstants.FILE_ONE, exclusiveToFile1.size(), summaryResponse);
        responseBuilder.setUnmatched(StringConstants.FILE_TWO, exclusiveToFile2.size(), summaryResponse);
        responseBuilder.setFileOneUnmatched(exclusiveToFile1);
        responseBuilder.setFileTwoUnmatched(exclusiveToFile2);

        final int id = RandomUtils.nextInt(10000);
        summaryResponse.setId(id);
        simpleCache.put(id,responseBuilder);
        logger.info("Summary Response: "+summaryResponse);
        return summaryResponse;


    }
    /**
     * Remove any duplicates from a list
     * @param listContainingDuplicates - the list to be inspected
     * @return - the duplicates
     */
    private Set<TransactionBean> removeDuplicates(final List<TransactionBean> listContainingDuplicates) {

        final Set<TransactionBean> setToReturn = new HashSet<>();
        final Set<TransactionBean> set1 = new HashSet<>();

        for (final TransactionBean transactionBean : listContainingDuplicates) {
            if (!set1.add(transactionBean)) {
                setToReturn.add(transactionBean);
                logger.fine("DUPLICATE FOUND: {}" + transactionBean);
            }
        }
        return setToReturn;
    }

    /**
     * Retrieve the file name from the map
     * @param header - the request header
     * @return - the file name
     */
    private String getFileName(final MultivaluedMap<String, String> header) {
        final String[] contentDisposition = header.getFirst(StringConstants.CONTENT_DISPOSITION).split(StringConstants.SEMI_COLON);
        String finalFileName = StringConstants.UNKNOWN;
        for (final String filename : contentDisposition) {
            if ((filename.trim().startsWith(StringConstants.FILENAME_PARAMETER))) {
                final String[] name = filename.split(StringConstants.EQUALS);
                finalFileName = name[1].trim().replaceAll(BACK_SLASH, "");
            }
        }
        logger.info("Retrieved filename: '" + finalFileName + "'");
        return finalFileName;
    }

}
