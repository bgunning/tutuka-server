package com.brian.app.utilities;

import com.brian.app.annotationinterfaces.IAppUtilities;
import com.brian.app.exceptions.UploadRequestException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.logging.Logger;

@Singleton
@IAppUtilities
public class AppUtilities {

    @SuppressWarnings("CdiInjectionPointsInspection")
    @Inject
    private ObjectMapper objectMapper;

    @Inject
    private Logger logger;

    /**
     * Converts the object to a JSON string using {@link ObjectMapper}
     * @param object - object to be converted to JSON
     * @return - object as a JSON String
     * @throws JsonProcessingException - exception if issue converting the object
     */
    public String convertViaJacksonToJson(final Object object) throws JsonProcessingException{

            return objectMapper.writeValueAsString(object);

    }

    /**
     * Generate Error message
     * @param message - Message to insert into reply describing the exception
     * @return - error message for user
     */
    public String buildErrorResponseFromString(final String message){

        return StringConstants.ERROR_JSON_FORMAT_PREAMBLE + message + StringConstants.JSON_CLOSE_BRACKET;
    }

    /**
     * Check file type is supported
     * @param header - file/http header
     * @return - true if CSV
     * @throws IOException - if unsupported file type
     */
    @SuppressWarnings("SameReturnValue")
    public boolean isCsvFile(final MultivaluedMap<String, String> header) throws UploadRequestException {
        final String fileType = header.getFirst(StringConstants.CONTENT_TYPE);

        switch(fileType){
            case StringConstants.TEXT_CSV:
            case StringConstants.EXCEL_CSV:
                return true;
            default:
                logger.severe(StringConstants.UNSUPPORTED_FILE_TYPE +fileType);
                throw new UploadRequestException(StringConstants.UNSUPPORTED_FILE_TYPE+"_"+fileType);

        }
    }
}
