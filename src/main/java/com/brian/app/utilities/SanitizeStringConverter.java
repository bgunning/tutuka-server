package com.brian.app.utilities;

import com.brian.app.utilities.StringConstants;
import com.univocity.parsers.conversions.Conversion;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extension class written for Univoicity to remove all spaces and special characters from
 * a string to improve comparisons. Possibly could look at RegEx class included with Univoicity as an alternative
 */
public class SanitizeStringConverter implements Conversion<String,String> {


    private static final Pattern pattern = Pattern.compile(StringConstants.REGEX_A_Z_A_Z0_9);

    /**
     * Remove all special characters and spaces
     * @param theString the string to be parsed
     * @return  - the sanatised string
     */
    @Override
    public String execute(final String theString) {

        String aString=theString;
        final Matcher match = pattern.matcher(aString);
        while (match.find()) {
            final String s = match.group();
            aString = aString.replaceAll(String.format(StringConstants.REGEX_PERCENTAGE_S, s), "");
        }
        return aString;
    }

    @Override
    public String revert(final String o) {
        return null;
    }
}
