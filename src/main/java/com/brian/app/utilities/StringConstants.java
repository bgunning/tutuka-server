package com.brian.app.utilities;

public class StringConstants {


    public static final String REGEX_A_Z_A_Z0_9 = "[^a-zA-Z0-9]";
    public static final String REGEX_PERCENTAGE_S = "\\%s";
    public static final String FILE_ONE = "file1";
    public static final String FILE_TWO = "file2";
    public static final String UNABLE_TO_LOCATE_RECORD = "unable to locate record";
    public static final String ERROR_JSON_FORMAT_PREAMBLE = "{\"error\" : \"";
    public static final String JSON_CLOSE_BRACKET = "\"}";
    public static final String HEALTH_JSON_FORMAT = "{\"healthy\" : \"true\",";
    public static final String SERVERTIME_JSON_FORMAT = "\"servertime\" : \"";
    public static final String UNKNOWN_ERROR = "unknown error";
    public static final String DATA_PROCESSING_ERROR = "data processing error";
    public static final String TEXT_CSV = "text/csv";
    public static final String EXCEL_CSV="application/vnd.ms-excel";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String UNSUPPORTED_FILE_TYPE = "Unsupported FileType: ";
    public static final String FILENAME_PARAMETER = "filename";
    public static final String UNKNOWN = "unknown";
    public static final String CONTENT_DISPOSITION = "Content-Disposition";
    public static final String SEMI_COLON = ";";
    public static final String EQUALS = "=";
    public static final String RECORD_ID_NOT_SET = "RECORD_ID_NOT_SET";
    public static final String ERROR_PROCESSING_JSON_MESSAGE = "ERROR PROCESSING JSON. MESSAGE:";
}
