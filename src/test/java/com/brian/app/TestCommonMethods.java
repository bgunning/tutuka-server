package com.brian.app;

import com.brian.app.beans.TransactionBean;

import java.util.Date;

public class TestCommonMethods {


    @SuppressWarnings("SameParameterValue")
    public static TransactionBean generateBean(final String myprofile, final Date date, final int amount, final String mynarrative, final String mydescription, final int id, final int type, final String myreference) {
        final TransactionBean bean = new TransactionBean();
        bean.setProfileName(myprofile);
        bean.setTransactionDate(date);
        bean.setTransactionAmount(amount);
        bean.setTransactionNarrative(mynarrative);
        bean.setTransactionDescription(mydescription);
        bean.setTransactionID(1L);
        bean.setTransactionType(1);
        bean.setTransactionID(id);
        bean.setTransactionType(type);
        bean.setWalletReference(myreference);

        return bean;
    }
}
