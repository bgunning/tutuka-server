package com.brian.app.beans;

import com.brian.app.TestCommonMethods;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

public class FuzzLogicTest {

    private TransactionBean bean1;
    private TransactionBean bean2;
    private TransactionBean bean3;
    private TransactionBean bean4;

    @Before
    public void setUp() {

        Date date = Calendar.getInstance().getTime();
        bean1 = TestCommonMethods.generateBean("myprofile", date, 1, "mynarrative", "mydescription", 1,1,"myreference");
        bean2 = TestCommonMethods.generateBean("myprofile", date, 1, "mynarrative", "mydescription",1,1, "myreference");
        date = Calendar.getInstance().getTime();
        bean3 = TestCommonMethods.generateBean("myprofile2", date, 10, "mbiglittlenarrative", "mydescription",1,1, "myreference");
        bean4 = TestCommonMethods.generateBean("myprofile22222", date, 1, "myffnarrative2", "mydescription",1,1, "myreference");



    }


    @Test
    public void testFuzzyEquals() {


        final int result2=FuzzySearch.ratio(bean1.toString(),bean2.toString());

        final int result3=FuzzySearch.ratio(bean1.toString(),bean3.toString());

        final int result4=FuzzySearch.ratio(bean1.toString(),bean4.toString());

       // Assert.assertEquals(72,result);
//        Assert.assertEquals(70,result4);
//        Assert.assertEquals(100,result2);
//        Assert.assertEquals(70,result3);


    }

}
