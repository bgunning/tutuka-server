package com.brian.app.beans;

import com.brian.app.TestCommonMethods;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("EmptyMethod")
public class TransactionBeanTest {

    private TransactionBean bean1;
    private TransactionBean bean2;
    private TransactionBean bean3;
    private TransactionBean bean4;

    @Before
    public void setUp() {

        final Date date = Calendar.getInstance().getTime();
        bean1 = TestCommonMethods.generateBean("myprofile", date, 1, "mynarrative", "mydescription",1,1, "myreference");
        bean2 = TestCommonMethods.generateBean("myprofile", date, 1, "mynarrative", "mydescription",1,1, "myreference");
        bean3 = TestCommonMethods.generateBean("myprofile", date, 1, "mynarrative", "mydescription",1,2, "myreference");
//        bean3 = TestCommonMethods.generateBean("myprofile2", date, 1, "mynarrative2", "mydescription", "myreference");
    }

    @After
    public void tearDown() {
    }


    @Test
    public void testEquals() {

        Assert.assertEquals(bean1, bean2);


    }

    @Test
    public void testHashCode() {
        Assert.assertEquals(bean1.hashCode(), bean2.hashCode());
        Assert.assertNotEquals(bean1.hashCode(), bean3.hashCode());

    }

    @Test
    public void testPartialEquals() {
        Assert.assertTrue(bean1.partialEquals(bean3));
        Assert.assertFalse(bean1.partialEquals(bean4));

    }

}