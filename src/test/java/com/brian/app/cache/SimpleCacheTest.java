package com.brian.app.cache;

import com.brian.app.TestUtils;
import com.brian.app.responses.ResponseBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;


@SuppressWarnings("ConstantConditions")
public class SimpleCacheTest {

    private final ResponseBuilder responseBuilder = new ResponseBuilder();

    private final SimpleCache cache = new SimpleCache();
    @SuppressWarnings("unchecked")
    private final
    HashMap<Integer,ResponseBuilder> simCache = new HashMap();


    @Before
    public void setUp() {
        TestUtils.overrideField(cache,"simCache",simCache);
        cache.put(22,responseBuilder);

    }

    @Test
    public void put() {

        cache.put(1,responseBuilder);
        final ResponseBuilder result = cache.get(1);
        Assert.assertTrue(result instanceof ResponseBuilder);
    }



    @Test
    public void get() {
        final ResponseBuilder result = cache.get(22);
        Assert.assertTrue(result instanceof ResponseBuilder);

    }
}