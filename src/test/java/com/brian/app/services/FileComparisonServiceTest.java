package com.brian.app.services;

import com.brian.app.TestUtils;
import com.brian.app.exceptions.ReconciliationRequestException;
import com.brian.app.services.algorithms.ReconciliationRequest;
import com.brian.app.services.algorithms.ReconciliationStrategyEnum;
import com.brian.app.services.handlers.ReconcileFileHandler;
import com.brian.app.services.handlers.UploadedFileHandler;
import com.brian.app.utilities.AppUtilities;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FileComparisonServiceTest{

    private static final int FUZZY_MATCH_NOT_REQUIRED = -1;
    
    @Mock
    private ReconcileFileHandler mockReconciliationFileHandler;

    @Mock
    private MultipartFormDataInput mockInput;

    @Mock
    private ReconciliationRequest mockReconciliationRequest;

    @Mock
    private UploadedFileHandler mockFileHandler;

    private FileComparisonService fileComparisonService;

    @Before
    public void setUp() {
        fileComparisonService = new FileComparisonService();
        TestUtils.overrideField(fileComparisonService, "reconciliationHandler", mockReconciliationFileHandler);
        TestUtils.overrideField(fileComparisonService, "reconciliationRequest", mockReconciliationRequest);
        TestUtils.overrideField(fileComparisonService, "fileHandler", mockFileHandler);
        TestUtils.overrideField(fileComparisonService, "logger", Logger.getLogger(FileComparisonService.class.getName()));
        TestUtils.overrideField(fileComparisonService, "appUtilities", new AppUtilities());
    }


    @Test
    public void getMatches_returnsResponseSuccess() {
        try {
            when(mockReconciliationFileHandler.reconcileFiles(any(ReconciliationRequest.class))).thenReturn("RECONCILE_SUCCESS");
            final Response resp = fileComparisonService.getMatches("1");
            assertEquals(resp.getStatus(), 200);
            assertTrue((resp.getEntity()).equals("RECONCILE_SUCCESS"));
            verifyReconciliationRequest(1, ReconciliationStrategyEnum.BASIC, FUZZY_MATCH_NOT_REQUIRED);
            verify(mockReconciliationFileHandler).reconcileFiles(mockReconciliationRequest);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test
    public void getMatches_errorThrownInFileReconciliation_returnsResponseFailure() {
        try {
            when(mockReconciliationFileHandler.reconcileFiles(any(ReconciliationRequest.class))).thenThrow(new ReconciliationRequestException("Error during reconcile"));
            final Response resp = fileComparisonService.getMatches("2");
            assertEquals(resp.getStatus(), 400);
            assertEquals("{\"error\" : \"Error during reconcile\"}", resp.getEntity());
            verifyReconciliationRequest(2, ReconciliationStrategyEnum.BASIC, FUZZY_MATCH_NOT_REQUIRED);
            verify(mockReconciliationFileHandler).reconcileFiles(mockReconciliationRequest);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test
    public void getMatches_errorThrownDueToNumberFormatError_returnsResponseFailure() {
        try {
            final Response resp = fileComparisonService.getMatches("abc");
            assertEquals(resp.getStatus(), 400);
            assertEquals("{\"error\" : \"unable to locate record\"}", resp.getEntity());
            verifyZeroInteractions(mockReconciliationRequest);
            verifyZeroInteractions(mockReconciliationFileHandler);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test
    public void test_getMatchesFuzzy_returnsResponseSuccess() {
        try {
            when(mockReconciliationFileHandler.reconcileFiles(any(ReconciliationRequest.class))).thenReturn("RECONCILE_SUCCESS");
            final Response resp = fileComparisonService.getMatchesFuzzy("1", 50);
            assertEquals(resp.getStatus(), 200);
            assertTrue((resp.getEntity()).equals("RECONCILE_SUCCESS"));
            verifyReconciliationRequest(1, ReconciliationStrategyEnum.FUZZY_LOGIC, 50);
            verify(mockReconciliationFileHandler).reconcileFiles(mockReconciliationRequest);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test
    public void test_getMatchesFuzzy_errorThrownInFileReconciliation_returnsResponseFailure() {
        try {
            when(mockReconciliationFileHandler.reconcileFiles(any(ReconciliationRequest.class))).thenThrow(new ReconciliationRequestException("Error during reconcile"));
            final Response resp = fileComparisonService.getMatchesFuzzy("2", 75);
            assertEquals(resp.getStatus(), 400);
            assertEquals("{\"error\" : \"Error during reconcile\"}", resp.getEntity());
            verifyReconciliationRequest(2, ReconciliationStrategyEnum.FUZZY_LOGIC, 75);
            verify(mockReconciliationFileHandler).reconcileFiles(mockReconciliationRequest);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test
    public void test_getMatchesFuzzy_errorThrownDueToNumberFormatError_returnsResponseFailure() {
        try {
            final Response resp = fileComparisonService.getMatchesFuzzy("abc", 100);
            assertEquals(resp.getStatus(), 400);
            assertEquals("{\"error\" : \"unable to locate record\"}", resp.getEntity());
            verifyZeroInteractions(mockReconciliationRequest);
            verifyZeroInteractions(mockReconciliationFileHandler);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test
    public void test_getMatchesByTransactionId_returnsResponseSuccess() {
        try {
            when(mockReconciliationFileHandler.reconcileFiles(any(ReconciliationRequest.class))).thenReturn("RECONCILE_SUCCESS");
            final Response resp = fileComparisonService.getMatchesByTransactionId("3");
            assertEquals(resp.getStatus(), 200);
            assertTrue((resp.getEntity()).equals("RECONCILE_SUCCESS"));
            verifyReconciliationRequest(3, ReconciliationStrategyEnum.TRANSACTION_ID, FUZZY_MATCH_NOT_REQUIRED
            );
            verify(mockReconciliationFileHandler).reconcileFiles(mockReconciliationRequest);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test
    public void test_getMatchesByTransactionId_errorThrownInFileReconciliation_returnsResponseFailure() {
        try {
            when(mockReconciliationFileHandler.reconcileFiles(any(ReconciliationRequest.class))).thenThrow(new ReconciliationRequestException("Error during reconcile"));
            final Response resp = fileComparisonService.getMatchesByTransactionId("4");
            assertEquals(resp.getStatus(), 400);
            assertEquals("{\"error\" : \"Error during reconcile\"}", resp.getEntity());
            verifyReconciliationRequest(4, ReconciliationStrategyEnum.TRANSACTION_ID, FUZZY_MATCH_NOT_REQUIRED);
            verify(mockReconciliationFileHandler).reconcileFiles(mockReconciliationRequest);
        } catch (final Exception exception) {
            fail();
        }
    }
    
    @Test
    public void test_getMatchesByTransactionId_errorThrownDueToNumberFormatError_returnsResponseFailure() {
        try {
            final Response resp = fileComparisonService.getMatchesByTransactionId("abc");
            assertEquals(resp.getStatus(), 400);
            assertEquals("{\"error\" : \"unable to locate record\"}", resp.getEntity());
            verifyZeroInteractions(mockReconciliationRequest);
            verifyZeroInteractions(mockReconciliationFileHandler);
        } catch (final Exception exception) {
            fail();
        }
    }

//    @Test
//    public void test_uploadFile_returnsResponseSuccess() {
//        try {
//            when(mockFileHandler.handleFile(mockInput)).thenReturn("FILE_HANDLED_SUCCESS");
//            final Response result = fileComparisonService.uploadFile(mockInput);
//            assertEquals(result.getStatus(), 200);
//            assertEquals("FILE_HANDLED_SUCCESS", result.getEntity());
//        } catch (final Exception exception) {
//            fail();
//        }
//    }
//
//    @Test
//    public void test_uploadFile_errorThrownDueToIOException_returnsResponseFailure() {
//        try {
//            when(mockFileHandler.handleFile(any(MultipartFormDataInput.class))).thenThrow(new IOException("IOException Error"));
//            final Response result = fileComparisonService.uploadFile(mockInput);
//            assertEquals(result.getStatus(), 400);
//            assertEquals("{\"error\" : \"data processing error\"}", result.getEntity());
//            verify(mockFileHandler).handleFile(mockInput);
//        } catch (final Exception exception) {
//            fail();
//        }
//    }
//
//    @Test
//    public void test_uploadFile_errorThrownDueToNullPointerException_returnsResponseFailure() {
//        try {
//            when(mockFileHandler.handleFile(any(MultipartFormDataInput.class))).thenThrow(new NullPointerException("NullPointerException Error"));
//            final Response result = fileComparisonService.uploadFile(mockInput);
//            assertEquals(result.getStatus(), 400);
//            assertEquals("{\"error\" : \"data processing error\"}", result.getEntity());
//            verify(mockFileHandler).handleFile(mockInput);
//        } catch (final Exception exception) {
//            fail();
//        }
//    }
//
//    @Test
//    public void test_uploadFile_errorThrownDueToDataProcessingException_returnsResponseFailure() {
//        try {
//            when(mockFileHandler.handleFile(any(MultipartFormDataInput.class))).thenThrow(new DataProcessingException("DataProcessingException Error"));
//            final Response result = fileComparisonService.uploadFile(mockInput);
//            assertEquals(result.getStatus(), 400);
//            assertEquals("{\"error\" : \"data processing error\"}", result.getEntity());
//            verify(mockFileHandler).handleFile(mockInput);
//        } catch (final Exception exception) {
//            fail();
//        }
//    }
//
//    @Test
//    public void test_uploadFile_errorThrownDueToGeneralException_returnsResponseFailure() {
//        try {
//            doThrow(new UploadRequestException("UploadRequestException Error")).when(mockFileHandler).handleFile(any(MultipartFormDataInput.class));
//            final Response result = fileComparisonService.uploadFile(mockInput);
//            assertEquals(result.getStatus(), 400);
//            assertEquals("{\"error\" : \"unknown error\"}", result.getEntity());
//            verify(mockFileHandler).handleFile(mockInput);
//        } catch (final Exception exception) {
//            fail();
//        }
//    }
    
    private void verifyReconciliationRequest(final int recordId, final ReconciliationStrategyEnum strategy, final int fuzzyTolerance) {
        verify(mockReconciliationRequest).setRecordId(recordId);
        verify(mockReconciliationRequest).setStrategy(strategy);
        if (fuzzyTolerance != FUZZY_MATCH_NOT_REQUIRED) {
            verify(mockReconciliationRequest).setFuzzyTolerance(fuzzyTolerance);
        }
    }

}