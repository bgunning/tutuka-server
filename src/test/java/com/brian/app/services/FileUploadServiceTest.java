package com.brian.app.services;

import com.brian.app.TestUtils;
import com.brian.app.services.FileUploadService;
import com.brian.app.services.handlers.ReconcileFileHandler;
import com.brian.app.services.handlers.UploadedFileHandler;
import com.brian.app.utilities.AppUtilities;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import com.brian.app.exceptions.UploadRequestException;
import com.univocity.parsers.common.DataProcessingException;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FileUploadServiceTest {

    @Mock
    private ReconcileFileHandler mockReconciliationFileHandler;

    @Mock
    private MultipartFormDataInput mockInput;

    @Mock
    private UploadedFileHandler mockFileHandler;

    private FileUploadService fileUploadService;

    @Before
    public void setUp() {
        fileUploadService = new FileUploadService();
        TestUtils.overrideField(fileUploadService, "reconciliationHandler", mockReconciliationFileHandler);
        TestUtils.overrideField(fileUploadService, "fileHandler", mockFileHandler);
        TestUtils.overrideField(fileUploadService, "logger", Logger.getLogger(FileUploadService.class.getName()));
        TestUtils.overrideField(fileUploadService, "appUtilities", new AppUtilities());
    }


    @Test
    public void test_uploadFile_returnsResponseSuccess() {
        try {
            when(mockFileHandler.handleFile(mockInput)).thenReturn("FILE_HANDLED_SUCCESS");
            final Response result = fileUploadService.uploadFile(mockInput);
            assertEquals(result.getStatus(), 200);
            assertEquals("FILE_HANDLED_SUCCESS", result.getEntity());
        } catch (final Exception exception) {
            exception.printStackTrace();
            fail();
        }
    }

    @Test
    public void test_uploadFile_errorThrownDueToIOException_returnsResponseFailure() {
        try {
            when(mockFileHandler.handleFile(any(MultipartFormDataInput.class))).thenThrow(new IOException("IOException Error"));
            final Response result = fileUploadService.uploadFile(mockInput);
            assertEquals(result.getStatus(), 400);
            assertEquals("{\"error\" : \"data processing error\"}", result.getEntity());
            verify(mockFileHandler).handleFile(mockInput);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test
    public void test_uploadFile_errorThrownDueToNullPointerException_returnsResponseFailure() {
        try {
            when(mockFileHandler.handleFile(any(MultipartFormDataInput.class))).thenThrow(new NullPointerException("NullPointerException Error"));
            final Response result = fileUploadService.uploadFile(mockInput);
            assertEquals(result.getStatus(), 400);
            assertEquals("{\"error\" : \"data processing error\"}", result.getEntity());
            verify(mockFileHandler).handleFile(mockInput);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test
    public void test_uploadFile_errorThrownDueToDataProcessingException_returnsResponseFailure() {
        try {
            when(mockFileHandler.handleFile(any(MultipartFormDataInput.class))).thenThrow(new DataProcessingException("DataProcessingException Error"));
            final Response result = fileUploadService.uploadFile(mockInput);
            assertEquals(result.getStatus(), 400);
            assertEquals("{\"error\" : \"data processing error\"}", result.getEntity());
            verify(mockFileHandler).handleFile(mockInput);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test
    public void test_uploadFile_errorThrownDueToGeneralException_returnsResponseFailure() {
        try {
            doThrow(new UploadRequestException("UploadRequestException Error")).when(mockFileHandler).handleFile(any(MultipartFormDataInput.class));
            final Response result = fileUploadService.uploadFile(mockInput);
            assertEquals(result.getStatus(), 400);
            assertEquals("{\"error\" : \"UploadRequestException Error\"}", result.getEntity());
            verify(mockFileHandler).handleFile(mockInput);
        } catch (final Exception exception) {
            exception.printStackTrace();
            fail();
        }
    }


}
