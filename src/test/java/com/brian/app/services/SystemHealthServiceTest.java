package com.brian.app.services;

import com.brian.app.utilities.StringConstants;

import com.brian.app.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class SystemHealthServiceTest {

    private SystemHealthService systemHealthService;

    @Before
    public void setUp() {
        systemHealthService = new SystemHealthService();
        TestUtils.overrideField(systemHealthService, "logger", Logger.getLogger(SystemHealthService.class.getName()));

    }



    @Test
    public void test_health_returnsResponseSuccess() {
        final Response result = systemHealthService.health();
        assertEquals(200, result.getStatus());
        assertTrue(((String)result.getEntity()).contains(StringConstants.HEALTH_JSON_FORMAT + StringConstants.SERVERTIME_JSON_FORMAT));
    }
}
