package com.brian.app.services.handlers;

import com.brian.app.TestUtils;
import com.brian.app.exceptions.ReconciliationRequestException;
import com.brian.app.services.algorithms.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ReconcileFileHandlerTest {

    private static final String RESPONSE__RECONCILE_FILES_BASIC = "RECONCILE_FILES_BASIC";
    private static final String RESPONSE__RECONCILE_FILES_FUZZY = "RECONCILE_FILES_FUZZY";
    private static final String RESPONSE__RECONCILE_FILES_TRANSACTION = "RECONCILE_FILES_TRANSACTION";
    
    @Mock private Logger mockLogger;
    @Mock private ReconcileByTransactionId mockReconcileByTransactionId;
    @Mock private ReconcileByColumnMatch mockReconcileByColumnMatch;
    @Mock private ReconcileByFuzzyLogic mockReconcileByFuzzyLogic;

    private final ReconcileFileHandler reconcileFileHandler = new ReconcileFileHandler();

    @Before 
    public void setup() throws Exception {
        TestUtils.overrideField(reconcileFileHandler, "logger", mockLogger);
        TestUtils.overrideField(reconcileFileHandler, "reconcileByTransactionId", mockReconcileByTransactionId);
        TestUtils.overrideField(reconcileFileHandler, "reconcileByColumnMatch", mockReconcileByColumnMatch);
        TestUtils.overrideField(reconcileFileHandler, "reconcileByFuzzyLogic", mockReconcileByFuzzyLogic);
        when(mockReconcileByTransactionId.reconcile(any(ReconciliationRequest.class))).thenReturn(RESPONSE__RECONCILE_FILES_TRANSACTION);
        when(mockReconcileByColumnMatch.reconcile(any(ReconciliationRequest.class))).thenReturn(RESPONSE__RECONCILE_FILES_BASIC);
        when(mockReconcileByFuzzyLogic.reconcile(any(ReconciliationRequest.class))).thenReturn(RESPONSE__RECONCILE_FILES_FUZZY);
    }

    @Test 
    public void test_reconcileFiles_reconcileStrategyBasic() {
        try {
            final String result = reconcileFileHandler.reconcileFiles(createReconciliationRequest(ReconciliationStrategyEnum.BASIC));
            assertEquals(RESPONSE__RECONCILE_FILES_BASIC, result);
            verify(mockReconcileByColumnMatch).reconcile(any(ReconciliationRequest.class));
            verifyZeroInteractions(mockReconcileByTransactionId);
            verifyZeroInteractions(mockReconcileByFuzzyLogic);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test 
    public void test_reconcileFiles_reconcileStrategyFuzzyLogic() {
        try {
            final String result = reconcileFileHandler.reconcileFiles(createReconciliationRequest(ReconciliationStrategyEnum.FUZZY_LOGIC));
            assertEquals(RESPONSE__RECONCILE_FILES_FUZZY, result);
            verifyReconcileCalledOnce(mockReconcileByFuzzyLogic);
            verifyReconcileNeverCalled(mockReconcileByTransactionId, mockReconcileByColumnMatch);
        } catch (final Exception exception) {
            fail();
        }
    }

    @Test 
    public void test_reconcileFiles_reconcileStrategyTransactionId() {
        try {
            final String result = reconcileFileHandler.reconcileFiles(createReconciliationRequest(ReconciliationStrategyEnum.TRANSACTION_ID));
            assertEquals(RESPONSE__RECONCILE_FILES_TRANSACTION, result);
            verifyReconcileCalledOnce(mockReconcileByTransactionId);
            verifyReconcileNeverCalled(mockReconcileByFuzzyLogic, mockReconcileByColumnMatch);
        } catch (final Exception exception) {
            fail();
        }
    }

    private ReconciliationRequest createReconciliationRequest(final ReconciliationStrategyEnum strategy) {
        final ReconciliationRequest request = new ReconciliationRequest();
        request.setStrategy(strategy);
        return request;
    }

    private void verifyReconcileCalledOnce(final ReconcileFiles reconcileFile) throws ReconciliationRequestException {
            verify(reconcileFile).reconcile(any(ReconciliationRequest.class));
    }
    
    private void verifyReconcileNeverCalled(final ReconcileFiles ... reconcileFiles) {
        for (final ReconcileFiles file : reconcileFiles) {
            verifyZeroInteractions(file);
        }
    }
}