package com.brian.app.services.handlers;


import com.brian.app.TestUtils;
import com.brian.app.cache.SimpleCache;
import com.brian.app.exceptions.UploadRequestException;
import com.brian.app.responses.ResponseBuilder;
import com.brian.app.responses.SummaryResponse;
import com.brian.app.utilities.AppUtilities;
import com.brian.app.utilities.StringConstants;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.util.GenericType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.*;
import java.io.*;
import java.lang.reflect.Type;
import java.util.*;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

/**
 * Test class for {@link UploadedFileHandler}
 */
@SuppressWarnings("SameParameterValue")
@RunWith(MockitoJUnitRunner.class) public class UploadedFileHandlerTest {

    private final String FILE_NAME_RELATIVE_PATH = "src/test/resources/";

    @Rule public final ExpectedException expectedException = ExpectedException.none();

    @Mock private SimpleCache simpleCacheMock;
    @Mock private Logger loggerMock;
    @Mock private SummaryResponse summaryResponseMock;
    @Mock private ResponseBuilder summaryResponseBuilderMock;
    @Mock private AppUtilities appUtilsMock;
    @Mock private MultipartFormDataInput multipartFormDataInputMock;

    private final UploadedFileHandler uploadedFileHandler = new UploadedFileHandler();

    @Before public void setup() {
        TestUtils.overrideField(uploadedFileHandler, "simpleCache", simpleCacheMock);
        TestUtils.overrideField(uploadedFileHandler, "logger", loggerMock);
        TestUtils.overrideField(uploadedFileHandler, "summaryResponse", summaryResponseMock);
        TestUtils.overrideField(uploadedFileHandler, "summaryResponseBuilder", summaryResponseBuilderMock);
        TestUtils.overrideField(uploadedFileHandler, "appUtils", appUtilsMock);
    }

    @Test public void test_handleFile_firstInputFileIsNull_NullPointerExceptionIsThrown() {
        try {
            expectedException.expect(NullPointerException.class);
            expectedException.expectMessage("Unable to parse file");

            final Map<String, List<InputPart>> paramsMap = new HashMap<>();
            paramsMap.put(StringConstants.FILE_ONE, null);
            paramsMap.put(StringConstants.FILE_TWO, Collections.singletonList(mock(InputPart.class)));
            when(multipartFormDataInputMock.getFormDataMap()).thenReturn(paramsMap);

            uploadedFileHandler.handleFile(multipartFormDataInputMock);
            verifyZeroInteractions(appUtilsMock.convertViaJacksonToJson(any(SummaryResponse.class)));
        } catch (final IOException | UploadRequestException exception) {
            fail();
        }
    }

    @Test public void test_handleFile_secondInputFileIsNull_NullPointerExceptionIsThrown() {
        try {
            expectedException.expect(NullPointerException.class);
            expectedException.expectMessage("Unable to parse file");

            final Map<String, List<InputPart>> paramsMap = new HashMap<>();
            paramsMap.put(StringConstants.FILE_ONE, Collections.singletonList(mock(InputPart.class)));
            paramsMap.put(StringConstants.FILE_TWO, null);
            when(multipartFormDataInputMock.getFormDataMap()).thenReturn(paramsMap);

            uploadedFileHandler.handleFile(multipartFormDataInputMock);
            verifyZeroInteractions(appUtilsMock.convertViaJacksonToJson(any(SummaryResponse.class)));
        } catch (final IOException | UploadRequestException exception) {
            fail();
        }
    }

    @SuppressWarnings("unchecked") @Test public void handleFile_fileOneEqualsFileTwo_ReturnsPositiveResponse() {
        try {
            when(appUtilsMock.isCsvFile(any(MultivaluedMap.class))).thenReturn(true);
            final String expectedResponse = createSummaryResponse("summary_response__fileOneAndTwoMatches.json");
            when(appUtilsMock.convertViaJacksonToJson(any(SummaryResponse.class))).thenReturn(expectedResponse);
            setupMultipartFormDataInputMock(createInputPart("fileOneMatchesFileTwo.csv"), createInputPart("fileTwoMatchesFileOne.csv"));

            final String result = uploadedFileHandler.handleFile(multipartFormDataInputMock);
            assertEquals(expectedResponse, result);

            verify(appUtilsMock).convertViaJacksonToJson(any(SummaryResponse.class));
        } catch (final IOException | ParseException | UploadRequestException exception) {
            fail();
        }
    }

    private String createSummaryResponse(final String summaryFileName) throws IOException, ParseException {
        final JSONParser jsonParser = new JSONParser();
        final Object object = jsonParser.parse(new FileReader(FILE_NAME_RELATIVE_PATH + summaryFileName));
        final JSONObject jsonObject = (JSONObject) object;
        return jsonObject.toJSONString();
    }

    private InputPart createInputPart(final String fileName) {
        return new TestInputPart(fileName);
    }

    private void setupMultipartFormDataInputMock(final InputPart inputPartOne, final InputPart inputPartTwo) {
        final Map<String, List<InputPart>> paramsMap = new HashMap<>();
        paramsMap.put(StringConstants.FILE_ONE, Collections.singletonList(inputPartOne));
        paramsMap.put(StringConstants.FILE_TWO, Collections.singletonList(inputPartTwo));
        when(multipartFormDataInputMock.getFormDataMap()).thenReturn(paramsMap);
    }

    /**
     * Utility Test class to allow verification of CSV file represented as an {@code InputPart} object
     */
    private class TestInputPart implements InputPart {

        private final String fileName;

        TestInputPart(final String fileName) {
            this.fileName = fileName;
        }

        @Override public MultivaluedMap<String, String> getHeaders() {
            final MultivaluedMap<String, String> multivaluedMap = new MultivaluedHashMap<>();
            multivaluedMap
                .put(StringConstants.CONTENT_DISPOSITION, Collections.singletonList("Content-Disposition: file; filename=\"" + fileName + "\""));
            return multivaluedMap;
        }

        @Override public String getBodyAsString() {
            return null;
        }

        @SuppressWarnings("unchecked") @Override public <T> T getBody(final Class<T> aClass, final Type type) throws IOException {
            final InputStream inputStream = new FileInputStream(FILE_NAME_RELATIVE_PATH + fileName);
            return (T) inputStream;
        }

        @Override public <T> T getBody(final GenericType<T> genericType) {
            return null;
        }

        @Override public MediaType getMediaType() {
            return MediaType.TEXT_PLAIN_TYPE;
        }

        @Override public boolean isContentTypeFromMessage() {
            return false;
        }
    }
}
