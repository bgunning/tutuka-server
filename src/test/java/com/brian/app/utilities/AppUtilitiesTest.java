package com.brian.app.utilities;

import com.brian.app.TestCommonMethods;
import com.brian.app.TestUtils;
import com.brian.app.beans.TransactionBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

@SuppressWarnings({"SameParameterValue", "EmptyMethod"})
public class AppUtilitiesTest {

    private AppUtilities utilities;
    private TransactionBean bean;
    private Date date;

    @Before
    public void setUp() {
        utilities = new AppUtilities();
//        bean= TestCommonMethods.generateBean("myprofile",date,1,"mynarrative","mydescription","myreference");
        bean = TestCommonMethods.generateBean("myprofile", date, 1, "mynarrative", "mydescription",1,1, "myreference");
        final ObjectMapper mapper = new ObjectMapper();
        TestUtils.overrideField(utilities,"objectMapper", mapper);

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testConvertViaJacksonToJson() {

        String result = null;
        final String expected= "{\"profileName\":\"myprofile\",\"transactionDate\":null,\"transactionAmount\":1,\"transactionNarrative\":\"mynarrative\",\"transactionDescription\":\"mydescription\",\"transactionID\":1,\"transactionType\":1,\"walletReference\":\"myreference\"}";

        try {

            result = utilities.convertViaJacksonToJson(bean);
            Assert.assertTrue(true);
        } catch (final JsonProcessingException e) {
            e.printStackTrace();
        }

        Assert.assertTrue(true);
        Assert.assertTrue(expected.equalsIgnoreCase(result));

    }


    @Test(expected = JsonProcessingException.class)
    public void testConvertViaJacksonToJsonException() throws JsonProcessingException {

        utilities.convertViaJacksonToJson(this);
    }


}