package com.brian.app.utilities;

import org.junit.Assert;
import org.junit.Test;

public class SanitiseStringConverterTest {


    private final SanitizeStringConverter converter = new SanitizeStringConverter();


    @Test
    public void testSanitizeStringConverterExecute() {
        final String testString = "hjd  g$h&jk8^i0s  sh6";
        final String expectedString ="xxyy";
        final String expectedString2="hjdghjk8i0ssh6";
        final String sanitizedString= converter.execute("xx&*&%%  yy");
        Assert.assertTrue(sanitizedString.equalsIgnoreCase(expectedString));

        final String sanitizedString2= converter.execute(testString);
        Assert.assertTrue(sanitizedString2.equalsIgnoreCase(expectedString2));
    }

}